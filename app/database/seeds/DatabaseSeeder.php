<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
            if (App::environment() === 'production') {
                exit('I just stopped you getting fired. Love Phil');
            }
            Eloquent::unguard();

		$this->call('UserTableSeeder');
                $this->call('ProvidersSeeder');
                $this->call('OrdersSeeder');
	}

}

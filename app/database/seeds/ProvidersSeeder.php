<?php


class ProvidersSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();

        $faker = Faker\Factory::create('uk_UA');

        try {
            $this->initTags($faker);
        } catch (PDOException $e) {}; // suppress unique tag value constraint's error during generation
        $this->initProviders($faker);
    }
    
    private function initTags(Faker\Generator $faker) {
        for ($i = 0; $i < Config::get('app.seeding.tags'); $i++) {
            Tag::create(array('value'=> $faker->unique()->word));
        }
    }
    
    private function initProviders (Faker\Generator $faker) {
        $enFaker = Faker\Factory::create();
        for ($i = 0; $i < Config::get('app.seeding.providers'); $i++) {
            $provider = Provider::create(array(
                'name' => $faker->word,
            ));
            $this->populateContacts($provider, $faker, $enFaker);
            
            for ($j = 0; $j< Config::get('app.seeding.maxTagsPerProvider'); $j++) {
                $this->populateTag($provider);
            }
        }
    }
    
    private function populateContacts(Provider $provider, Faker\Generator $ruFaker, Faker\Generator $enFaker) {
        $contacts = ['addresses'=> [], 'phones'=>[], 'emails'=> [], 'skype'=> []];
        for ($j = 0; $j < Config::get('app.seeding.maxContactsPerProvider'); $j++) {
            if (rand(1,2) == 1) {continue;} // we will get different amount of contacts for each provider
            switch (rand(1,4)) {
                case 1: // address
                    $contacts['addresses'][] = $ruFaker->address;
                    break;
                case 2: // phone
                    $contacts['phones'][] = $enFaker->PhoneNumber;
                    break;
                case 3: //email
                    $contacts['emails'][] = 'E-mail: ' . $enFaker->email;
                    break;
                default: // skype
                    $contacts['skype'][] = 'skype: ' . $enFaker->firstName . '_' . $enFaker->optional()->lastName;
            }
        }
        $provider->contacts = implode("\n", array_flatten([$contacts['addresses'], $contacts['phones'], $contacts['emails'], $contacts['skype']]));
        $provider->save();
    }
    
    private function populateTag(Provider $provider) {
        try {
            $provider->tags()->attach(Tag::orderByRaw("RANDOM()")->first()->id);
        } catch (Exception $e) {
            //suppress primary key constraint violation if any
        }
    }

}

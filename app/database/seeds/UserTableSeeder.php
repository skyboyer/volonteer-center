<?php


class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();
            
        $enFaker = Faker\Factory::create();
        $uaFaker = Faker\Factory::create('uk_UA');

        for ($i = 0; $i < Config::get('app.seeding.users'); $i++) {
            $email = $enFaker->email;
            $contacts = [];
            //TODO refactor this: providerSeeder has the same logic
            for ($j = 0; $j < Config::get('app.seeding.maxContactsPerProvider'); $j++) {
                if (rand(0, 1) > 0.5) {
                    continue;
                } // we will get different amount of contacts for each provider
                switch (rand(1, 4)) {
                    case 1: // address
                        $contacts[] = $uaFaker->address;
                        break;
                    case 2: // phone
                        $contacts[] = $enFaker->PhoneNumber;
                        break;
                    case 3: //email
                        $contacts[] = 'E-mail: ' . $enFaker->email;
                        break;
                    default: // skype
                        $contacts[] = 'skype: ' . $enFaker->firstName . '_' . $enFaker->optional()->lastName;
                }
            }
            $referalUser = User::orderByRaw("RANDOM()")->first();

            User::create(array(
                        'name' => $enFaker->firstName . ' ' . $enFaker->lastName,
                        'email' => $email,
                        'password' => Hash::make($email),
                        'contacts' => implode("\n", $contacts),
                        'referal_user_id' => $referalUser? $referalUser->id: -1,
            ));
        }
    }
}
    
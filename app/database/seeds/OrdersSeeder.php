<?php
class OrdersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Eloquent::unguard();

        $faker = Faker\Factory::create('ru_RU');

        $this->initRecipients($faker);
        $this->initOrders($faker);
        Order::all()->each(function ($order) use ($faker) {
            return $this->initStatusesFor($order, $faker);
        });
    }

    private function initRecipients(Faker\Generator $faker) {
        for ($i = 0;$i < Config::get('app.seeding.recipients');$i++) {
            Recipient::create(['name'=> $faker->unique()->numerify('%# рота %# бригады')]);
        }
    }

    private function initOrders(Faker\Generator $faker) {
        for ($i = 0; $i < Config::get('app.seeding.orders'); $i++) {
            Order::create([
                'title' => $faker->randomElement(['Тактические шлемы', "Тактические очки", "Тактически перчатки", "Комбинезон маскировочный", "Бронежилет"]),
                'amount' => $faker->optional()->randomNumber(1, 2000),
                'priority' => $faker->optional()->randomNumber(1,5),
                'recipient_id' => Recipient::orderByRaw("RANDOM()")->first()->id
            ]);
        }
    }
    
    private function initStatusesFor(Order $order, Faker\Generator $faker) {
        $prev = Status::create([
            'order_id' => $order->id,
            'targetType' => 'WAITING',
            'initiator_id' => User::orderByRaw("RANDOM()")->first()->id,
            'when' => time()
        ]);
        for ($i = 0; $i< Config::get('app.seeding.maxStatusesPerOrder') - 1; $i++) {
            if (rand(0, 10)< 2) {
                $prev = Status::create([
                    'order_id' => $order->id,
                    'targetType' => 'CANCELLED',
                    'initiator_id' => User::orderByRaw("RANDOM()")->first()->id,
                    'when' => time()
                ]);
            } else if (rand(0, 10) > 3) {
                $possibleNextTypes = array_filter(['WAITING', 'ORDERED', 'PAYED', 'STORED', 'SENT', 'RECEIVED'], function ($statusType) use ($prev) {
                    return Status::isStateChangeAllowed($prev->targetType, $statusType) 
                            && $statusType != $prev->order->getCurrentStatus()->targetType;
                });                
                if (empty($possibleNextTypes)) break; // CANCELLED status cannot be overrided
                $prev = Status::create([
                    'order_id' => $order->id,
                    'targetType' => $faker->randomElement($possibleNextTypes),
                    'initiator_id' => User::orderByRaw("RANDOM()")->first()->id,
                    'when' => time()
                ]);
            }
        }        
    }
}
    
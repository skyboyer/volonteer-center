<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UniqueTags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tags', function ($table) {
                    $table->unique(['value'], 'tagsUniqueValue');
                });
		Schema::table('tags_normalized', function ($table) {
                    $table->unique(['value'], 'tagsNormalizedUniqueValue');
                });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::table('tags', function ($table) {
                $table->dropUnique('tagsUniqueValue');
            });
            Schema::table('tags_normalized', function ($table) {
                $table->dropUnique('tagsNormalizedUniqueValue');
            });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("tags_normalized", function ($table) {
                    $table->increments('id');
                    $table->string('value');
                });
		Schema::create("tags", function ($table) {
                    $table->increments('id');
                    $table->string('value');
                });
                Schema::create("tags_to_normalized_tags", function ($table) {
                    $table->integer('tag_id');
                    $table->integer('normalized_tag_id');
                    $table->primary(['tag_id', 'normalized_tag_id']);
                });
        }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("tags_normalized");
		Schema::drop("tags");
		Schema::drop("tags_to_normalized_tags");
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class ContactsAsOneField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('providers', function ($table) {
                    $table->text('contacts')->nullable();
                });
                $oldContacts = DB::table("contacts")->orderBy('provider_id')->orderBy('type')->get();
                $currentProviderId = 0;
                $contacts = [];
                foreach($oldContacts as $contact) {
                    if (!$currentProviderId) {
                        $currentProviderId = $contact->provider_id;
                    } else if ($currentProviderId != $contact->provider_id) {
                        DB::table("providers")->where('id', '=', $currentProviderId)->update(['contacts' => implode("\n", $contacts)]);
                        $contacts = [];
                        $currentProviderId = $contact->provider_id;
                    }
                    switch ($contact->type) {
                        case 'skype':
                            $prefix = 'skype: ';
                            break;
                        case 'email':
                            $prefix = 'email: ';
                            break;
                        default:
                            $prefix = '';
                    }
                    $contacts[] = $prefix . $contact->value;
                } 
                if (!empty($contacts)) {
                    DB::table("providers")->where('id', '=', $currentProviderId)->update(['contacts' => implode("\n", $contacts)]);
                }
                Schema::drop('contacts');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            /*Schema::create('contacts', function($table) {
                $table->increments('id');
                $table->enum('type', array('phone', 'email', 'address', 'skype'));
                $table->integer('provider_id');
                $table->string('value');
                $table->timestamps();
            });*/
            
            $providers = DB::table("providers")->get();
            foreach ($providers as $provider) {
                $contacts = explode("\n", $provider->contacts);       
                foreach ($contacts as $contact) {
                    switch (true) {
                        case(preg_match('#e.?mail.*@#i', $contact)):
                            $type = 'email';
                            break;
                        case(preg_match('#skype#i', $contact)):
                            $type = 'skype';
                            break;
                        case(preg_match('#^[0-9()\s-]+$#i', $contact)):
                                $type = 'phone';
                                break;
                        default:
                            $type = 'address';
                    }
                    DB::table("contacts")->insert([
                        'type' => $type, 
                        'value'=> $contact, 
                        'provider_id' => $provider->id, 
                        'created_at'=> new Carbon,
                        'updated_at'=> new Carbon
                    ]);
                }                        
            }
            Schema::table('providers', function ($table) {
                $table->dropColumn('contacts');
            });
    }

}

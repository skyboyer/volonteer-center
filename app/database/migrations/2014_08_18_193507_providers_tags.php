<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProvidersTags extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create("providers_tags", function ($table) {
                $table->integer('provider_id');
                $table->integer('tag_id');
                $table->primary(['provider_id', 'tag_id']);
            });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop("providers_tags");
	}

}

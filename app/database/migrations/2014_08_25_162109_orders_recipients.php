<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersRecipients extends Migration {

        private function makeAttachmentsTable (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('mime');
            $table->unique(['path']);
            $table->timestamps();
            $table->softDeletes();
        }
    
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('recipients', function ($table) {
                $table->increments('id');
                $table->string('name');
                $table->text('description')->default('');
                $table->unique(['name']);
                $table->timestamps();
                $table->softDeletes();
            });
            Schema::create('orders', function ($table) {
                $table->increments('id');
                $table->integer('recipient_id');
                $table->string('title');
                $table->float('amount')->nullable();
                $table->text('description')->nullable();
                $table->integer('assignee_id')->nullable();
                $table->integer('priority')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            Schema::create('order_files', function ($table) {
                $this->makeAttachmentsTable($table);
                $table->integer('order_id');
            });
            Schema::create('statuses', function ($table) {
                $table->increments('id');
                $table->integer('order_id');
                $table->string('targetType');
                $table->integer('initiator_id');
                $table->timestamp('when');
                $table->text('comment')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
            Schema::create('status_files', function ($table) {
                $this->makeAttachmentsTable($table);
                $table->integer('status_id');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recipients');
		Schema::drop('orders');
		Schema::drop('order_files');
		Schema::drop('statuses');
		Schema::drop('status_files');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProvidersContacts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::create('providers', function($table) {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            });
            Schema::create('contacts', function($table) {
                $table->increments('id');
                $table->enum('type', array('phone', 'email','address','skype'));
                $table->integer('provider_id');
                $table->string('value');
                $table->timestamps();
            });            
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('providers');
            Schema::drop('contacts');
            Schema::drop('providers_contacts');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoftDeletes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
            Schema::table('providers', function ($table) {
                $table->softDeletes();
            });
            Schema::table('contacts', function ($table) {
                $table->softDeletes();
            });
            Schema::table('users', function ($table) {
                $table->softDeletes();
            });
        }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::table('providers', function ($table) {
                $table->dropSoftDeletes();
            });
            Schema::table('contacts', function ($table) {
                $table->dropSoftDeletes();
            });
            Schema::table('users', function ($table) {
                $table->dropSoftDeletes();
            });
	}

}

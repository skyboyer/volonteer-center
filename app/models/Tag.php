<?php
/**
 * incapsulates tag<->[normalized tags list]
 * all you need to do is only instantiate Tag with appropriate value and it will be linked with normalized tags automatically
 * $tag = Tag::firstOrNew(['value'=>"loooong loooooooong looooooooooong string with many values"]);
 * $tag->save();
 * 
 * Be aware of implict instatiation: it is better to use static method firstOrNew because tags' values are also unique in the DB
 * So with implict instatiating you have a chance to get "unique constraint error" during saving
 */
class Tag extends Eloquent {
    
    protected $fillable = ['value'];
    public $timestamps = false;
    
    public function normalized() {
        return $this->belongsToMany('TagNormalized', 'tags_to_normalized_tags', 'tag_id', 'normalized_tag_id');
    }
    
    public static function boot() {
        parent::boot();

        self::saved(function ($tag) {
            $tag->onSave();
        });
    }

    private function onSave() {
        // clean out old links
        $this->normalized()->detach($this->normalized()->getRelatedIds());
        $tokens = Normalization::splitToWords($this->value);
        foreach($tokens as $token) {
            $value = Normalization::isAbbrevation($token)? $token: Normalization::normalize($token);
            $normalizedTag = TagNormalized::firstOrCreate(['value'=> $value]);
            $normalizedTag->save();
            $this->normalized()->attach($normalizedTag);
        }
    }
}

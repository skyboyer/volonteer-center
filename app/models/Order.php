<?php

class Order extends Eloquent {
    use SoftDeletingTrait;
    protected $guarded = [];


    public function __get($name) {
        if (substr($name, 0, 2) === 'is') {
            $statusName = strtolower(substr($name, 2));
            $possibleStatuses = [];
            $impossibleStatuses = [];
            switch ($statusName) {
                case 'active':
                    $impossibleStatuses = [Status::$cancelled_status, Status::$received_status];
                    break;
                case 'closed':
                    $possibleStatuses = [Status::$cancelled_status, Status::$received_status];
                    break;
                default:
                    $possibleStatuses = [strtoupper($statusName)];
            }
            if (!empty($possibleStatuses) && !in_array($this->getCurrentStatus()->targetType, $possibleStatuses)) {
                return false;
            }
            if (!empty($impossibleStatuses) && in_array($this->getCurrentStatus()->targetType, $impossibleStatuses)) {
                return false;
            }
            return true;
        } else {
            return parent::__get($name);
        }
    }
    
    public function statuses () {
        return $this->hasMany('Status');
    }
    
    public function attachments () {
        return $this->hasMany('OrderAttachment');
    }
    
    public function recipient () {
        return $this->belongsTo('Recipient');
    }
    
    public static function searchByName ($namePart) {
        return static::query()->where('name', 'like', '%'. $namePart.'%')->get();
    }
    
    public function getCurrentStatus() {
        return $this->statuses()->orderBy('id', 'desc')->first();
    }
    
    public function assignee() {
        return $this->getCurrentStatus()->assignee;
    }
    
    public function getRecipientName() {
        return $this->recipient->name;
    }
}
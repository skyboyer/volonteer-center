<?php

class StatusAttachment extends AbstractAttachment {
    protected $table = 'status_files';
    public static function getDestinationPath() {
        return base_patch() . '/' . Config::get('app.statusAttachmentsPath', '') . '/';
    }

    public function status () {
        return $this->belongsTo('Status');
    }
}

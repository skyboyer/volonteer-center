<?php

abstract class AbstractAttachment extends Eloquent {
    use SoftDeletingTrait;
    
    abstract public static function getDestinationPath();
    
    public static function saveFile($index, $parentId) {
        if (!Input::hasFile($index) || !Input::file($index)->isValid()) return null;
        $obj = new static();
        $obj->path = uniqid($parentId);
        $file = Input::file($index);
        $file->move(static::getDestinationPath(), $obj->path);
        $file->mime = $file->getMimeType();
        return $obj;
    }
}
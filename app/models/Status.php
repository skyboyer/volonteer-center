<?php

class Status extends ValidableModel {
    use SoftDeletingTrait;
    protected $guarded = [];
    public static $waiting_status = 'WAITING';
    public static $ordered_status = 'ORDERED';
    public static $payed_status = 'PAYED';
    public static $stored_status = 'STORED';
    public static $sent_status = 'SENT';
    public static $received_status = 'RECEIVED';
    public static $cancelled_status = 'CANCELLED';
    
    public function attachments () {
        return $this->hasMany('StatusAttachment');
    }
    
    public function assignee () {
        return $this->belongsTo('User', 'assignee_id');
    }
    
    public function order () {
        return $this->belongsTo('Order');
    }
    
    public function assignToMe () {
        $this->assignee()->associate(User::find(Auth::id()));
        return $this;
    }
        
    private static $typeChangesMap = [
        'WAITING' => ['ORDERED', 'CANCELLED', 'STORED'],
        'ORDERED' => ['CANCELLED', 'PAYED', 'WAITING', 'STORED'],
        'PAYED' => ['CANCELLED', 'WAITING', 'STORED'],
        'STORED' => ['CANCELLED', 'SENT'],
        'SENT' => ['CANCELLED', 'RECEIVED', 'WAITING'],
        'RECEIVED' => [],
        'CANCELLED' => [],
    ];
    
    public static function isStateChangeAllowed($old, $new) {
        if (empty($old)) return $new !== self::$cancelled_status; // there are no sense to create order in immediatly cancelled state
        if ($old == $new) return true; // the same status can be used to re-assign or to put additional comments
        return !empty(self::$typeChangesMap[$old]) 
            && in_array($new, self::$typeChangesMap[$old]);
    }
    
    public function validate () {
        return static::isStateChangeAllowed(
                $this->order->getCurrentStatus()? $this->order->getCurrentStatus()->targetType: null,
                $this->targetType
        );
    }
    
    public static function getAllStatuses ($currentStatus = '') {
        return array_map(function (&$status) use ($currentStatus) {
            $status['enabled'] = self::isStateChangeAllowed($currentStatus, $status['key']);
            return $status;
        }, [
            ['key'=> self::$waiting_status, 'title'=> 'Заявка!'],
            ['key'=> self::$ordered_status, 'title'=> 'Заказано'],
            ['key'=> self::$payed_status, 'title'=> 'Оплачено'],
            ['key'=> self::$stored_status, 'title'=> 'На складе'],
            ['key'=> self::$sent_status, 'title'=> 'Отправлено'],
            ['key'=> self::$received_status, 'title'=> 'Получено'],
            ['key'=> self::$cancelled_status, 'title'=> 'Отменено :('],
        ]);
    } 
    
}

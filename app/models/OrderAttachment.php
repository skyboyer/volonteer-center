<?php
class OrderAttachment extends AbstractAttachment {
    protected $table = 'order_files';
    public static function getDestinationPath() {
        return base_patch() . '/' . Config::get('app.orderAttachmentsPath', '') . '/';
    }
    
    public function order() {
        return $this->belongsTo('Order');
    } 
    
}
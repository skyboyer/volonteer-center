<?php

class Recipient extends Eloquent {
    use SoftDeletingTrait;
    protected $guarded = [];
            
    public function orders () {
        return $this->hasMany('Order');
    }
    
    public static function searchByName ($query) {
        return static::with('statuses')->where('name', 'like', '%'.$query.'%')->get();
    }
}

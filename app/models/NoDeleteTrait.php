<?php

/**
 * Provide stab for Eloquent's delete method to prevent model from being deleted
 */
trait NoDeleteTrait {
    public function delete() {
        // do nothing
    }
}

<?php

class Provider extends Eloquent {

    use SoftDeletingTrait;
        
    public function tags() {
        return $this->belongsToMany('Tag', 'providers_tags', 'provider_id', 'tag_id');
    }
    
    public function getTagsAsString ($delimiter = ', ') {
        return implode(
            $delimiter, 
            $this->tags->map(
                function ($tag) {
                    return $tag->value;
                }
            )->toArray()
        );
    }
    
    public function setTagsFromString ($tagsJoined, $tagsDelimiter = ',') {
        $this->tags()->sync(
            array_filter(
                array_map(function ($tagValue) {
                        $tagValue = trim($tagValue);
                        if (!$tagValue) return;
                        return Tag::firstOrCreate(['value' => $tagValue])->getKey();
                    }, 
                    explode($tagsDelimiter, $tagsJoined)
                )
            )
        );
    }
    
    public static function search ($query) {
        $providersList = self::with('tags');
        if (!empty($query)) {
            $ids = [];
            $searchResult = (new Sphinx\SphinxClient())->query($query, 'providers_plain');
            if (!empty($searchResult['matches'])) {
                $ids = array_keys($searchResult['matches']);
            }
            $providers = $providersList->whereIn('id', $ids);
        }
        return $providersList->get();
    }
}
<?php

/*
 * built from the http://stackoverflow.com/questions/18199756/laravel-4-trouble-overriding-models-save-method answers
 */
abstract class ValidableModel extends Eloquent {
    /**
     * Error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected $errors;

    /**
     * Listen for save event
     */
    protected static function boot() {
        parent::boot();

        static::saving(function($model) {
            return $model->validate();
        });
    }
    
    /**
     * Validates models as well as sets error messages to $errors bag
     * returns {Boolean} true if model is valid, false otherwise
     */
    abstract public function validate();


    /**
     * Set error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected function setErrors($errors) {
        $this->errors = $errors;
    }

    /**
     * Retrieve error message bag
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * Inverse of wasSaved
     */
    public function hasErrors() {
        return !empty($this->errors);
    }

}


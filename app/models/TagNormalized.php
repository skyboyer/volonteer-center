<?php

class TagNormalized extends Eloquent {
    use NoDeleteTrait;
    protected $table = 'tags_normalized';
    protected $fillable = ['value'];
    public $timestamps = false;

}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::pattern('id', '[0-9]+');

Route::get('login', array('as' => 'login', 'uses' => 'UserController@getLogin'));
Route::post('login', array('as' => 'post-login', 'uses' => 'UserController@postLogin'));
Route::get('logout', array('as' => 'logout', 'uses' => 'UserController@getLogout'))->before('auth');

Route::group(array('before' => 'auth'), function() {
    Route::Controller('providers', 'ProviderController');
    Route::Controller('orders', 'OrderController');
});

Validator::extend('structure', function ($attr, $value, $params) {
    if (!is_array($value)) return false;
    $rules = [];
    array_walk($params, function ($rule) use (&$rules) {
        $parts = explode('=', $rule);
        $rules[$parts[0]] = str_replace(['/', '*', '-'], ['|', ':', ','], $parts[1]);
    });
    return Validator::make($value, $rules)->passes();
});

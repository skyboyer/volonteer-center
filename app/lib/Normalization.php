<?php

class Normalization {
    public static function normalize ($word) {
        return $word;
    }
    
    public static function splitToWords ($string) {
        // TODO clarify why preg_split('#[\s\p{p}]+#' does not work
        return array_filter(explode(' ', str_replace(array(',', '.', ':', ';', '!', '?'), '', $string)));
    }
    
    /**
     * Checks for pattern "upper case letters, digits and -./ delimeters only" 
     * to detect something like "CGR-20" or something like this;
     * This should be useful to omit stemming and other normalization from being applied on appropriate lexem
     * @param type $string
     * @return boolean true if string given is abbrevation, false otherwise
     */
    public static function isAbbrevation ($string) {
        return !!preg_match('#^[\p{Lt}\d/.-]+$#u', $string);
    }
}

@extends('base')

@section('content')
    {{ Form::open(array('url' => $target)) }}
    {{ Form::hidden('originaName', $originalName) }}
    {{ Form::label('name', 'Название(без ООО, ОАО и прочих Inc)') }}
    {{ Form::text('name', $name) }}
    <br />
    <span style="color: #F00">{{{ $errors->first('name') }}}</span>
    <br />
    {{ Form::textarea('tags', $tags) }}
    <br />
    {{ Form::textarea('contacts', $contacts) }}
    <br />
    <span style="color: #F00">{{{ $errors->first('contacts') }}}</span>
    <br />
    {{ Form::submit('Сохранить', ['name'=>"action"]) }}
    {{ Form::close() }}
@stop
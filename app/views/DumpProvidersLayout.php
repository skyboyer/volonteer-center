<?xml version = "1.0" encoding = "utf-8" ?>
<sphinx:docset>

    <sphinx:schema>
        <sphinx:field name="name"/>
        <sphinx:field name="contacts"/>
        <sphinx:field name="tags"/>
    </sphinx:schema>
<?php foreach($providers as $provider) { ?>
    <sphinx:document id="<?=$provider->id;?>">
        <name><![CDATA[[<?= $provider->name; ?>]]></name>
        <contacts><![CDATA[[<?=$provider->contacts;?>]]>
        </contacts>
        <tags><![CDATA[[<?= $provider->getTagsAsString(" "); ?>]]></tags>
    </sphinx:document>
<?php } ?>
</sphinx:docset>
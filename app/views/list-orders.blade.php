@extends('base')

@section('content')
{{ Form::open(['method' => 'GET']) }}
<table>
    <thead>
        <tr>
            <th>Кому</th>
            <th>Что</th>
            <th>Сколько</th>
            <th>Статус</th>
            <th>Приоритет</th>
        </tr>
        <tr>
            <th>{{ Form::text('recipientLike', '') }}</th>
            <th>{{ Form::text('titleLike', '') }}</th>
            <th>{{ Form::text('amountMoreThan', '') }}</th>
            <th></th>
            <th>{{ Form::text('priorityMoreThan', '') }}</th>
        </tr>
    </thead>
    <tbody>
@foreach ($orders as $order)        
        <tr>
            <td>{{{ $order->recipient->name }}}</td>
            <td>{{{ $order->title }}}</td>
            <td>{{{ $order->amount? number_format($order->amount): '<сколько получится>' }}}</td>
            <td>{{{ $order->getCurrentStatus()->targetType }}}</td>
            <td>{{{ $order->priority }}}</td>
        </tr>
@endforeach
    </tbody>
</table>
{{ Form::submit() }}
{{ Form::close() }}
@stop
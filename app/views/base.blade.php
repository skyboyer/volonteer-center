<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>
            @section('title')
            {{{ $title }}}
            @show            
        </title>
    </head>
    <body>
        @yield('content')
    </body>
</html>
@extends('base')


<?php
function error($errors, $messageIndex) {
    if (!empty($errors->get($messageIndex))) {
        return '<div style="color: red;">'.implode('<br />',$errors->get($messageIndex)).'</div>';
    }
}
?>

@section('content')

{{ Form::open(['url' => URL::action("OrderController@anyAdd"), 'method' => 'POST']) }}
@foreach($statuses as $status)
{{ Form::radio('status', $status['key'], $status['key'] === Status::$waiting_status, array_merge(['id' => 'status-'.$status['key']],  $status['enabled']? []: ['disabled' => 'disabled'])) }}
{{ Form::label('status-'.$status['key'], $status['title'], $status['enabled'] ? [] : ['disabled' => 'disabled']) }}
<br />
@endforeach
{{ Form::label('recipient', 'Кому:') }}
{{ Form::text('recipient', $recipient) }}
{{ error($errors, 'recipient') }}
<br />
{{ Form::label('title', 'Что:') }}
{{ Form::text('title', $title) }}
{{ error($errors, 'title') }}
<br />
{{ Form::label('amount', 'Как много:') }}
{{ Form::text('amount', $amount) }}
{{ error($errors, 'amount') }}
<br />
{{ Form::submit("Добавить") }}
{{ Form::close() }}
    
@stop
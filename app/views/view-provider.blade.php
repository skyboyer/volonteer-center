@extends('base')

@section('content')
<a href="{{ URL::action('ProviderController@getList') }}">к списку</a>
<br />
<h2>{{{ $provider->name }}} (<a href="{{ URL::action('ProviderController@anyEdit', ['id'=> $provider->id]) }}">ред.</a>)</h2>

<h3>Контакты</h3>
{{ nl2br($provider->contacts) }}
        
<h3>Теги</h3>
{{{ $provider->getTagsAsString() }}}
@stop
@extends('base')

@section('content')
{{ Form::open(['method'=> 'GET', 'url' => URL::action('ProviderController@getList')]) }}
{{ Form::label('search', 'Что искать?') }}
{{ Form::text('search', $query) }}
{{ Form::submit("Вперед!") }}
{{ Form::close() }}
@foreach($providers as $provider)
    <a href="{{ URL::action('ProviderController@getShow', ['id'=> $provider->id]) }}">{{{ $provider->name }}}</a><br />
@endforeach
@stop
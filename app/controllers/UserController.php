<?php

class UserController extends BaseController {
    
    public function getLogin() {
        if (Auth::guest()) {
            return View::make('login-form');
        } else {
            echo 'you have already logged in';
            return;
        }
    }
    
    public function postLogin() {
        $user = User::where(Input::except('action', '_token'))->first();
        if ($user) {
            Auth::login($user, true);
            return Redirect::to('/orders/list');
        } else {
            return Redirect::to('/login');
        }
    }
        
    public function getLogout() {
        Auth::logout();
        return Redirect::to('/login');
    }
    
}

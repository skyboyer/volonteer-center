<?php
function compareBy($a, $b, $keys) {
    foreach ($keys as $key) {
        $valueA = $a;
        $valueB = $b;
        foreach(explode('->', $key) as $subkey) {
            $valueA = $valueA{$subkey};
            $valueB = $valueB{$subkey};
        }
        if ($valueA == $valueB) continue;
        return ($valueA> $valueB)? +1: -1;
    }
    return 0;
}

function filterOrdersByFlag ($orders, $flagName) {
    return $orders->filter(function ($order) use ($flagName) {
        return $order{$flagName};
    });
}

function stringEndsWith($haystack, $needle) {
    return strpos(strrev($haystack), strrev($needle)) === 0;
}

function applyInputFilters ($orders, $inputs) {
    $operatorsMapping = [
        'MoreThan' => '>', 
        'LessThan' => '<',
        'Is' => '=',
        'Equals' => '=',
        'Like' => 'like'
    ];
    foreach ($inputs as $key => $value) {
        if (empty($value)) continue;
        $operator = '=';
        foreach ($operatorsMapping as $suffix => $operatorMapped) {
            if (stringEndsWith($key, $suffix)) {
                $operator = $operatorMapped;
                $key = substr($key, 0, -(strlen($suffix)));
                break;
            }
        }
        if ($operator == 'like' && strpos($value, '%') === false) {
            $value = '%' . $value . '%';
        }
        switch ($key) {
            case 'recipient':
                $orders = $orders->whereHas('recipient', function ($query) use ($operator, $value) {
                    return $query->where('name', $operator, $value);
                });
                break;
            default:
                $orders = $orders->where($key, $operator, $value);
        }
    }
    return $orders;
}

class OrderController extends BaseController {
    
    private static $validationRules = [
        'recipient' => 'required',
        'title' => 'required',
        'amount' => 'numeric',
        'initialStatus' => 'required|in:STORED,ORDERED,PAIED,SENT',
    ];
    
    private static $validationMessages = [
        'recipient.required' => 'Надо указать получателя',
        'title.required' => 'Укажите, пожалуйста, наименование',
        'amount.numeric' => 'Укажите ориентировочное количество или оставьте пустым для "как можно больше" и "сколько успеем"',
        'initialStatus.reduired' => 'Статус не верный! Как вам это удалось?',
        'initialStatus.in' => 'Статус не верный! Как вам это удалось?',
        
    ];
    
    public function getList($filterType = 'all', $layoutType = 'list') {
        Input::flash();
        $orders = applyInputFilters(Order::with('statuses'), Input::all()); 
                
        $orders = $orders->get();
        switch (strtolower($filterType)) {
            case 'all': 
                break;
            default:
                $orders = filterOrdersByFlag($orders, 'is' . ucfirst($filterType));
        }

        switch ($layoutType) {
        case 'list': // passthrough
        default:
            /*
                next block throws warning uasort(): Array was modified by the user comparison function 
             * it can be because of lazy loading(see http://www.doctrine-project.org/jira/browse/DDC-1592) or because of 
             * php issue with exceptions throwsing/catching deep inside of laravel/eloquent/doctrine(see https://bugs.php.net/bug.php?id=50688)
             * but as far as sorting still works we just supress warnings here
            */
            $orders = @$orders->sort(function ($order1, $order2) {
                return -compareBy($order1, $order2, ['isActive', 'recipient->name', 'priority']);
            });
        }
                
        return View::make($layoutType . '-orders')->with('orders', $orders)->with('title', 'Заявки');
    }
    
    public function anyAdd() {
        $recipientName = Input::old("recipient", Input::get("recipient", ''));
        $title = Input::old("title", Input::get("title", ''));
        $amount = Input::old("amount", Input::get("amount", ''));
        $startStatus = strtoupper(Input::old("status", Input::get("status", Status::$ordered_status)));
        $validator = Validator::make([
                    'recipient' => $recipientName,
                    'title' => $title,
                    'amount' => $amount,
                    'initialStatus' => $startStatus
                        ], self::$validationRules, self::$validationMessages);

        if ('POST' === Request::method()) {
            if ($validator->fails()) {
                return Redirect::to(URL::action('OrderController@anyAdd'))->
                                withErrors($validator)->
                                withInput();
            } else {
                $recipient = Recipient::firstOrCreate(['name'=> $recipientName]);
                $order = Order::create([
                    'title'=> $title,
                    'amount'=> $amount,
                    'recipient_id' => $recipient->id
                ]);
                $status = Status::create([
                    'order_id' => $order->id,
                    'targetType' => $startStatus,
                    'initiator_id' => Auth::id(),
                    'when' => time(),
                ])->assignToMe()->save();
                return Redirect::to(URL::action('OrderController@getList'));
            }
        }
        
        return View::make('add-order')->
                with('statuses', Status::getAllStatuses())->
                with('recipient', '')->
                with('title', '')->
                with('amount', '');
    }
    
    public function anyEdit($orderId) {
    }
}
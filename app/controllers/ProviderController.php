<?php

class ProviderController extends BaseController {
    private static $validationRules = [
        'name' => 'required',
        'contacts' => 'required'
    ];
    
    private static $validationMessages = [
        'name.required' => 'Нужно ввести название',
        'contacts.required' => 'Нужен хотя бы один контакт'
    ];

    public function getList() {
        $query = Input::get('search', '');
        return View::make('list-providers')->
                with('providers', Provider::search($query))->
                with('query', $query)->
                with('title', empty($query)?'Поставщики; все подряд': 'Поиск поставщиков');
    }
    
    public function getShow ($id) {
        $wordsToHighlight = Input::get('hl', []);
        $provider = Provider::with('tags')->find($id);
        return View::make('view-provider')->
                with('provider', $provider)->
                with('title', $provider->name)->
                with('hightlight', $wordsToHighlight);
    }
    
    public function anyAdd() {
        return $this->changeProvider(new Provider(), '', 'Добавить поставщика', URL::action('ProviderController@anyAdd'));
    }
    
    public function anyEdit($id) {
        $provider = Provider::with('tags')->find($id);
        return $this->changeProvider($provider, $provider->name, 'Изменить поставщика', URL::action('ProviderController@anyEdit', $id));
    }
    
    private function changeProvider (Provider $provider, $originalName, $title, $targetUrl) {
        $providerName = Input::old("name", Input::get("name", $originalName));
        $tagsString = Input::old("tags", Input::get("tags", $provider->getTagsAsString()));
        $contacts = Input::old("contacts", Input::get("contacts", $provider->contacts));
        $validator = Validator::make([
                    'name' => $providerName,
                    'contacts' => $contacts
                        ], self::$validationRules, self::$validationMessages);

        if ('POST' == Request::getMethod()) {
            if ($validator->fails()) { // something is wrong in form data
                return Redirect::to($targetUrl)->
                                withErrors($validator)->
                                withInput();
            }

            // everything is fine. saiving...
            $this->fillAndSave($provider, $providerName, $contacts, $tagsString);
            // ... and let's go to the providers list page
            return Redirect::action('ProviderController@getShow', ['id'=> $provider->id]);
        }
        return View::make('change-provider')->
                        with('originalName', $originalName)->
                        with('title', $title)->
                        with('target', $targetUrl)->
                        with('name', $providerName)->
                        with('tags', $tagsString)->
                        with('contacts', $contacts);
    }
    
    private function fillAndSave (Provider $provider, $providerName, $contacts, $tagsString) {
        $provider->name = $providerName;
        $provider->contacts = $contacts;
        $provider->save();
        $provider->setTagsFromString($tagsString);
    }
}
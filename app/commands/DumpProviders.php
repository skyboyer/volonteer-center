<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DumpProviders extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'providers:dump';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Dumps all providers with contacts and related tags to standard output in Sphinx-indexable format';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
            switch (strtolower($this->option('format'))) {
            case 'xml':
            default:
                echo View::make("DumpProvidersLayout")->with('providers', Provider::with('tags')->get());
            }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('format', 'f', InputOption::VALUE_OPTIONAL, 'Output type. Currently only XML is supported.', 'xml'),
		);
	}

}
